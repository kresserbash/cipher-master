#include <string.h>
#include <stdlib.h>

#ifndef GENLIB
#include "./genlib.h"
#define GENLIB
#endif

float* freq (char *text, char stream)
{
	int i, chars, letters = 0, size = strlen(text);

	if (stream)
	{
		int chars = 128, count[chars];
		float* frequency = (float*)malloc(chars*sizeof(float));

		for (i = 0; i < chars; i++) //Counting vector initialization
		{
			count[i] = 0;
		}
		for (i = 0; i < size; i++) //Just count every character using it's integer value
		{
			count[text[i]]++;
		}
		return frequency;
	}
	else
	{
		int chars = 26, count[chars];
		float* frequency = (float*)malloc(chars*sizeof(float));

		for (i = 0; i < chars; i++) //Counting vector initialization
		{
			count[i] = 0;
		}
		for (i = 0; i < size; i++)
		{
			if (isletter(text[i])) //If it's a letter, we count
			{
				letters++;
 				if (text[i] < 91) //High case letter
				{
					count[text[i]-65]++;
				}
				else //Low case letter
				{
					count[text[i]-97]++;
				}
			}
		}
		return frequency;
	}
}