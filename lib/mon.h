#include <time.h>


#ifndef GENLIB
#include "./genlib.h"
#define GENLIB
#endif

void genkey (char* key, char stream) //Generates a pseudo-random alphabet
{
	srand(time(NULL)); //Random seed

	int max, value, j = 0, i = 0, lrange;
	if (stream)
	{
		lrange = 0;
		max = 128;
	}
	else
	{
		lrange = 97;
		max = 26;
	}
	while(i < max)
	{
		value = lrange+rand()%max; //Generate a pseudo random value
		while(j < i && value != key[j]) //Verifies if it has not been used yet
		{
			j++;
		}
		if (j == i) //If reaches last j position, it's okay to use the new character
		{
			key[i++] = value;
		}
		j = 0;
	}
}

char substitute (char letter, char* key, char stream)
{
	if (stream)
	{
		return key[letter];
	}
	else
	{
		if (isletter(letter))
		{
			if (letter < 91) //High case letter
			{
				return key[letter-65];
			}
			else //Low case letter
			{
				return key[letter-97];
			}
		}
		return letter;
	}
}