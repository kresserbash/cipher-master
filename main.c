/*
	Name: Cipher-Master
	Description: Encripts a given stream using an arbitrary encription algorithm.
	Version: 0.0.1
	Usage: cipher [_options_] [_file_]
	The entry must be a stream of chars, followed by '/n'.
*/

#include <stdio.h>

#include "./lib/caesar.h"
#include "./lib/freq.h"
#include "./lib/mon.h"

int main (void)
{
	char letter[103], stream;
	int i = 0, max;

	printf("Stream mode? ");
	scanf(" %d", &stream);

	stream = stream%2;

	if (stream)
	{
		max = 128;
		char * key = (char *)malloc(max*sizeof(char));
		caesar(key, 3, stream);
		for (i = 0; i < max; i++)
		{
			printf("%c becomes %c\n", i, key[i]);
		}
	}
	else
	{
		max = 26;
		char * key = (char *)malloc(max*sizeof(char));
		caesar(key, 5, stream);
		for (i = 0; i < max; i++)
		{
			printf("%c becomes %c\n", i+65, key[i]);
		}
	}

	printf("texto: ");
	scanf(" %c", &letter[i++]);
	while (letter[i-1] != EOF)
	{
		scanf("%c", &letter[i++]);
	}


	return 0;
}